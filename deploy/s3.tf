resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files-8cf51786"
  acl           = "public-read"
  force_destroy = true
}
